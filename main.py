# Write a function that accepts a positive integer. We will perform a series of operations on that input until we terminate.
#
# While the input is greater than 1, we will divide it by two if it is/ even, otherwise, we will multiply it by 3 and add 1.
#
# We will do this until we reduce the input value to 1. The final output should be all the integers we see between the input and 1, inclusive.

def reduce(input: int, output: list) -> list:
    if input == 1:
        return output
    if not isinstance(input, int):
        raise ValueError("Input must be an integer")
    if input < 0:
        raise ValueError("Input must be a positive number")
    if input % 2 == 0:
        input //= 2
    else:
        input = input * 3 + 1
    output.append(input)
    return reduce(input, output)


def foo():
    res = reduce(4, [])
    print(res)

foo()
