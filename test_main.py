import pytest
from main import reduce


@pytest.mark.parametrize("input,expected", [
    (4, [4, 2, 1]),
    (5, [5, 16, 8, 4, 2, 1]),
    (0, [0, 1])

])
def test_reduce(input, expected):
    output = reduce(input, [])
    assert output == expected


@pytest.mark.parametrize("input", [(4.5),(-1)])
def test_reduce_exceptions(input):
    with pytest.raises(ValueError):
        reduce(input, [])
